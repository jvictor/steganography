#include<stdio.h>
#include<string.h>

int main(int argc, char * argv[]){
		char * file = argv[1];
		char * ofile = argv[2];
		char buffer[2];
		char * str = argv[3];
		char strarr[strlen(str)];
		char buf0;
		char buf1;
		int fileSize;
		int start;
		int width = 0;
		int height = 0;
		int byte = 0;
		int secretbit = 0;
		int secretbyte = 0;
		FILE * in_file = fopen(file, "r");
		FILE * out_file = fopen(ofile, "w");
		str = argv[3];
		fseek(in_file, 0x02, SEEK_SET);
		fread(&fileSize, sizeof(int), 1, in_file);
		fseek(in_file, 0x0A, SEEK_SET);
		fread(&start, sizeof(int), 1, in_file);
		fseek(in_file, 0x12, SEEK_SET);
		fread(&width, sizeof(int), 1, in_file);
		fseek(in_file, 0x16, SEEK_SET);
		fread(&height, sizeof(int), 1, in_file);
		fseek(in_file, 0, SEEK_SET);
		strncpy(strarr, str, sizeof(strarr));
		for (int i = 1; i <= fileSize; i++){
				if(i>=start && secretbyte<=strlen(str)){
						if((i-start)%4 == 3){
								fread(&buffer[0], sizeof(char), 1, in_file);
								fwrite(&buffer[0], 1, sizeof(char), out_file);
										if (byte == 4){
												byte = 0;
										}
										byte++;
						}else{
								fread(&buffer[0], sizeof(char), 1, in_file);
								buffer[0] = buffer[0]&0xFE;
								buffer[1] = strarr[secretbyte]<<secretbit;
								buffer[1] = buffer[1]&0x01;
								buffer[0] = buffer[0]|buffer[1];
								fwrite(&buffer[0], 1, sizeof(char), out_file);
								secretbit++;
						}if(secretbit >= 8){
								secretbyte++;
								secretbit = 0;
						}
				}else{
						fread(&buffer[0], sizeof(char), 1, in_file);
						fwrite(&buffer[0], sizeof(char), 1, out_file);
				}

		}
		fclose(in_file);
		fclose(out_file);
		printf("The input file path is %s\n", file);
		printf("The file size of the image is: %d bytes\n", fileSize);
		printf("The pixels start at: %d\n", start);
		printf("The width of the image is: %d\n", width);
		printf("The height of the image is: %d\n", height);
		printf("The output file path is %s\n", ofile);
		printf("The secret message is ");
		int count =0;
		for (int i=0; i<strlen(str); i++){
				printf("%c", strarr[i]);
				count++;
		}
		printf("\n");
		printf("count is %d\n", count);
		return 0;
}
